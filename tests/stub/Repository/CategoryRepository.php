<?php


namespace R2Soft\Database\Repository;


use R2Soft\Database\Models\Category;
use R2Soft\Database\AbstractRepository;

class CategoryRepository extends AbstractRepository
{

    public function model()
    {
        return Category::class;
    }

}
