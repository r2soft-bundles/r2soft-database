<?php


namespace R2Soft\Database\Criteria;


use Illuminate\Database\Eloquent\Model;
use R2Soft\Database\Contracts\CriteriaInterface;
use R2Soft\Database\Contracts\RepositoryInterface;

class FindByDescription implements CriteriaInterface
{

    private $description;

    public function __construct($description)
    {
        $this->description = $description;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('description', $this->description);
    }
}
