<?php

namespace R2Soft\Database\Criteria;

use Illuminate\Database\Eloquent\Model;
use R2Soft\Database\Contracts\CriteriaInterface;
use R2Soft\Database\Contracts\RepositoryInterface;

class Offset implements CriteriaInterface
{

    private $offset;
    private $limit;

    public function __construct($page, $limit = 5000)
    {
        $this->offset = $limit*($page-1);
        $this->limit = $limit;
    }

    /**
     * @param Model $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if($this->offset >= 0){
            return $model->offset($this->offset)->limit($this->limit);
        }
        return $model;
    }
}


