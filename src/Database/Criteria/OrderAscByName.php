<?php


namespace R2Soft\Database\Criteria;


use R2Soft\Database\Contracts\CriteriaInterface;
use R2Soft\Database\Contracts\RepositoryInterface;

class OrderAscByName implements CriteriaInterface
{

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orderBy('name', 'asc');
    }
}