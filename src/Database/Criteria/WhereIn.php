<?php


namespace R2Soft\Database\Criteria;


use R2Soft\Database\Contracts\CriteriaInterface;
use R2Soft\Database\Contracts\RepositoryInterface;

class WhereIn implements CriteriaInterface
{

    private $field;
    private $data;

    public function __construct($field, $data)
    {
        $this->field = $field;
        $this->data = $data;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model , RepositoryInterface $repository)
    {
        return $model->whereIn($this->field,  $this->data);
    }
}