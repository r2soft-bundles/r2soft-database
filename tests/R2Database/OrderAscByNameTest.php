<?php


namespace R2Soft\Database\Tests;


use Illuminate\Database\Eloquent\Builder;
use R2Soft\Database\Contracts\CriteriaInterface;
use R2Soft\Database\Criteria\OrderAscByName;
use R2Soft\Database\Models\Category;
use R2Soft\Database\Repository\CategoryRepository;

class OrderAscByNameTest extends AbstractTestCase
{

    /**
     * @var CategoryRepository
     */
    private $repository;

    /**
     * @var OrderAscByName
     */
    private $criteria;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new CategoryRepository();
        $this->criteria = new OrderAscByName();
        $this->createCategory();
    }

    public function test_if_instanceoff_criteriainterface() {
        $this->assertInstanceOf(CriteriaInterface::class, $this->criteria);
    }

    public function test_if_apply_returns_querybuild() {
        $class = $this->repository->model();
        $result = $this->criteria->apply(new $class, $this->repository);
        $this->assertInstanceOf(Builder::class, $result);
    }

    public function test_if_apply_returns_data() {
        $class = $this->repository->model();
        $result = $this->criteria->apply(new $class, $this->repository)->get();
        $this->assertEquals('Category 1', $result[0]->name);
        $this->assertEquals('Category 2', $result[1]->name);
    }

    private function createCategory(){
        Category::create([
            'name'=> 'Category 2',
            'description'=> 'Description 2'
        ]);
        Category::create([
            'name'=> 'Category 1',
            'description'=> 'Description 1'
        ]);
        Category::create([
            'name'=> 'Category 3',
            'description'=> 'Description 3'
        ]);
    }

}