<?php

namespace R2Soft\Database\Criteria;

use Illuminate\Database\Eloquent\Model;
use R2Soft\Database\Contracts\CriteriaInterface;
use R2Soft\Database\Contracts\RepositoryInterface;

class Limit implements CriteriaInterface
{

    private $limit;

    public function __construct($limit)
    {
        $this->limit = $limit;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        if($this->limit)
            return $model->limit($this->limit);
        return $model;
    }
}


